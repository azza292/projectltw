﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoadManager{

    static string levelExt = "-levelData1.ltw";
    static string playerExt = "PlayerData1.ltw";


    // ==============================================================================
    // LEVEL DATA
    // ==============================================================================

	public static void SaveLevelData(float winTime, int medal,int checkPointCount,bool[] howCoin, string level)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/" + level + levelExt, FileMode.Create); // FILE PATH
        LevelData data = new LevelData(winTime, medal, checkPointCount, howCoin); // CREATE NEW DATA CLASS
        bf.Serialize(stream, data); // SAVE DATA
        stream.Close(); // CLOSE DATA STREAM
    }

    public static float[] LoadLevelData(string level)
    {
        if(File.Exists(Application.persistentDataPath + "/" + level + levelExt)) // IF FILE IS THERE
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/" + level + levelExt, FileMode.Open); // FILE PATH
            LevelData data = bf.Deserialize(stream) as LevelData; // LOAD AS CORRECT TYPE
            stream.Close();// CLOSE DATA  STREAM

            return data.levelDataF; // RETURN LOADED DATA
        }
        else
        {
            return new float[5]; // RETURN EMPTY ARRAY
        }
    }

    // ==============================================================================
    // PLAYER DATA
    // ==============================================================================

    public static void SavePlayerData(int coinsCollected)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/" + playerExt, FileMode.Create); // FILE PATH
        PlayerData data = new PlayerData(coinsCollected); // CREATE NEW DATA CLASS
        bf.Serialize(stream, data); // SAVE DATA
        stream.Close(); // CLOSE DATA STREAM
    }

    public static int[] LoadPlayerData()
    {
        if (File.Exists(Application.persistentDataPath + "/" + playerExt)) // IF FILE IS THERE
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/" + playerExt, FileMode.Open); // FILE PATH
            PlayerData data = bf.Deserialize(stream) as PlayerData; // LOAD AS CORRECT TYPE
            stream.Close();// CLOSE DATA  STREAM

            return data.playerDataF; // RETURN LOADED DATA
        }
        else
        {
            return new int[1]; // RETURN EMPTY ARRAY
        }
    }
}

[Serializable]
public class LevelData {

    public float[] levelDataF;
	public LevelData(float winTime, int medal, int checkPointCount, bool[] howCoin)
    {
        levelDataF = new float[5];
        levelDataF[0] = winTime;
        levelDataF[1] = medal;
		levelDataF [2] = checkPointCount;
		levelDataF [3] = Convert.ToInt16 (howCoin [0]); // Speedy
		levelDataF [4] = Convert.ToInt16 (howCoin [1]); // Relentless Torch
    }
}

[Serializable]
public class PlayerData
{

    public int[] playerDataF;
    public PlayerData(int coinsCollected)
    {
        playerDataF = new int[1];
        playerDataF[0] = coinsCollected;
    }
}