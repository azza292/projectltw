﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OnTouchJS : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{


    private VirtualJoystick joyStick;
    private CanvasGroup joyStickCanvas;
    // Use this for initialization
    void Start()
    {

        joyStick = GetComponentInChildren<VirtualJoystick>(); // grabs virtual joystick
        joyStickCanvas = joyStick.GetComponent<CanvasGroup>(); // grabs canvas object for transparency 
    }

    public void OnPointerDown(PointerEventData ped)
    {
        joyStick.GetComponent<RectTransform>().position = ped.position; // spawn js on touch pos

        joyStickCanvas.alpha = 1f;
        joyStickCanvas.blocksRaycasts = true;
    }
    public void OnPointerUp(PointerEventData ped)
    {
        joyStick.inputVector = Vector3.zero;
        joyStick.joystickImg.rectTransform.anchoredPosition = Vector3.zero;

        joyStickCanvas.alpha = 0f;
        joyStickCanvas.blocksRaycasts = false;
    }
    public void OnDrag(PointerEventData ped)
    {
        joyStick.OnDrag(ped);
    }
}
