﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    private PauseController pause;
	private FollowPlayer mainCam;

    public string noteTitle;
    [TextArea(5,5)]
    public string noteContents;

	public Transform cameraTag;
	public float camFOV = 45f;

    private void Start()
    {
		if (cameraTag != null) {
			mainCam = Camera.main.GetComponent<FollowPlayer> ();
		}

        pause = FindObjectOfType<PauseController>();

        if (pause == null) Debug.LogError("Note: No pause controller found");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            gameObject.SetActive(false);
            pause.ShowNote(noteTitle, noteContents);
			if (cameraTag != null) {


				mainCam.HightlightObject (cameraTag, camFOV);
			}
        }
    }
}
