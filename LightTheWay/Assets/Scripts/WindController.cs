﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindController : MonoBehaviour {

    public Transform targetPoint;
    public Transform startObject;
    public float delayOnSpawn;
	public float prevTimeToTarget = 10f;
	private float currentTimeToTarget;


	public AudioClip[] windSounds;
	private AudioSource audioSrc;

    private Vector3 startPoint;
    private int cycleCount;

    private UnityEngine.AI.NavMeshAgent navAgent;
    // Use this for initialization
    void Start () {
		
        startPoint = startObject.transform.position;
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        navAgent.destination = targetPoint.position;

		audioSrc = GetComponent<AudioSource> ();

    }
	
	// Update is called once per frame
	void Update () {
		
		PlaySounds ();
		if(targetPoint && navAgent.enabled)
        {
            currentTimeToTarget += Time.deltaTime;
            
        }
	}

	private void PlaySounds()
	{
		if (!audioSrc.isPlaying) {
			audioSrc.PlayOneShot (windSounds [Random.Range (0, windSounds.Length)]);
		}
	}
    private void OnCollisionEnter(Collision colObject)
    {
        //Debug.Log("Wind Hit:  " + colObject.gameObject.name);
        if (colObject.gameObject.tag == "WindTarget")
        {
            prevTimeToTarget = currentTimeToTarget;
            currentTimeToTarget = 0;
            navAgent.enabled = false;
            transform.position = startPoint;
            StartCoroutine(Spawn(delayOnSpawn)); // Enable nav agent with time

        }
    }

    IEnumerator Spawn(float time)
    {
        yield return new WaitForSeconds(time);
        navAgent.enabled = true;
        navAgent.destination = targetPoint.position;
    }
}
