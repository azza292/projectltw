﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate_Collisions : MonoBehaviour {


	//
	// THIS CLASS PASSED THE COLLISION FROM PARENT TO CHILD
	//
	private PressurePlate target;
	void Start()
	{
		target = GetComponentInChildren<PressurePlate> ();

	}
	private void OnTriggerEnter(Collider other)
	{
		target.OnTriggerEnter (other);
	}
	private void OnTriggerStay(Collider other)
	{
		target.OnTriggerStay (other);
	}
	private void OnTriggerExit(Collider other)
	{
		target.OnTriggerExit (other);
	}
}
