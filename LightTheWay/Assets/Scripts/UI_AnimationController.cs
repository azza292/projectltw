﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_AnimationController : MonoBehaviour {

	public bool debug;

	public Animator mainNote;
	public Animator topBar;
	public Animator botBar;
	public Animator middleArea;
	public Animator levelSelect;
	public Animator settingsMenu;
	public Animator noteUI;


	void PlayAnim(string anim, Animator mator)
	{
		if (mator != null) {
			mator.Play (anim);
		} else {
			if(debug)Debug.Log ("FAIL: " +mator + anim);
		}
	}

	public void SlideIn()
	{
		PlayAnim("GoUp", botBar);
		PlayAnim("GoDown", topBar);
		PlayAnim ("FallDown", middleArea);
	}

	public void SlideOut()
	{

		PlayAnim("GoDown", botBar);
		PlayAnim("GoUp", topBar);
		PlayAnim ("FlyUp", middleArea);
	}
	// MAIN MENU
	public void MainMenuOpen()
	{
		PlayAnim ("Open", mainNote);
	}
	public void MainMenuClose()
	{
		PlayAnim ("Close", mainNote);
	}


	// LEVEL SELECT
	public void LevelSelectOpen()
	{
		PlayAnim ("Open", levelSelect);
	}
	public void LevelSelectClose()
	{
		PlayAnim ("Close", levelSelect);
	}


	// SETTINGS
	public void SettingsOpen()
	{
		PlayAnim ("Open", settingsMenu);
	}
	public void SettingsClose()
	{
		PlayAnim ("Close", settingsMenu);
	}

	// NOTE IN GAME
	public void OpenNote()
	{
		PlayAnim ("readNote", noteUI);
	}
	public void CloseNote()
	{
		PlayAnim ("closeNote", noteUI);
	}


}
