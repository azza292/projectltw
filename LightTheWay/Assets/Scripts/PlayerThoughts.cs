﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerThoughts : MonoBehaviour {

    // Use this for initialization
    private UI_TEXT ui;
    private GameController game;

    private float timePassedSeconds;
    private int actualLitCount, checkPointCount;

    public string[] wind = { "Curse the wind!", "I need to light my torch again", "Ahhh"};
    public string[] lastCP = { "All the fires are alight, but will they stay that way?", "I pray the gusts are no more" };
    public string[] obtainLight = { "I still have to light ", "Only "};
    public string[] doorOpen = { "This Key came in handy!", "Good thing found the key"};
    public string[] doorLocked = { "It won't move", "It must be locked", "Where is the key?"};
    public string[] woodenBox = { "What is this here for?", "This is heavy"};
    public string[] pressurePlate = { "I must find something to keep this pressed", "What can I place here?"};
    public string[] gateRise = { "Rise!", "It opens!" };
    public string[] gameWin = { "This branch of the castle is lit, the king will be pleased", "I did it! On to the next" };
    public string[] keyPickup = { "Shiny!", "A Key!" };
    public string[] metalGate = { "I can't go through here, but the wind can" , "Damn my big bones" };
	public string[] ironGate = { "Nothing gets through here...", "There must be a way to open this" };


    void Start () {

        game = GetComponent<GameController>();
        ui = GetComponent<GameController>().ui;

	}

    public void thoughtGenerator(int thoughtID) // chooses the thought to display and sends to UI
    {
        timePassedSeconds = game.timePassedSeconds;
        actualLitCount = game.actualLitCount;
        checkPointCount = game.checkPointCount;

        float thoughtTime = timePassedSeconds;
        string genThought = "";

        switch (thoughtID)
        {
		case 1: // WIND RELATED

			genThought = ChooseThought(wind , -1);
			break;

		case 2: //CHECKPOINT RELATED (LIGHT)

			if (actualLitCount + 1 == checkPointCount) //If its the last one lit
			{ genThought = ChooseThought(lastCP, -1); }
			else { genThought = (NumberToWords(actualLitCount + 1) + " down, " + NumberToWords(checkPointCount - 1 - actualLitCount) + " to go"); }
			break;

		case 3: //CHECKPOINT RELATED (OBTAIN LIGHT)
			if(!game.hasWon) genThought = ( ChooseThought(obtainLight, -1) + NumberToWords(checkPointCount - actualLitCount) + " more");
			break;

		case 4: // DOOR RELATED (OPEN)

			genThought = ChooseThought(doorOpen, -1);
			break;

		case 5:// DOOR RELATED (LOCKED)

			genThought = ChooseThought(doorLocked, -1);
			break;

		case 6://WOODEN BOX RELATED (MOVEABLE)

			genThought = ChooseThought(woodenBox, -1);
			break;

		case 7: //PRESSURE PLATE STAND ON

			genThought = ChooseThought(pressurePlate, -1);
			break;

		case 8: // PRESSURE PLATE COMPLETE

			genThought = ChooseThought(gateRise, -1);
			break;

		case 9: // KEY PICKUP

			genThought = ChooseThought(keyPickup, -1);
			break;

		case 10: // METAL GATE

			genThought = ChooseThought(metalGate, -1);
			break;

		case 11: // IRON GATE

			genThought = ChooseThought(ironGate, -1);
			break;

        case 99: // WIN RELATED

			genThought = ChooseThought(gameWin, -1);
			break;

        default:
			break;
        }
			
		ui.updateThoughtDisplay(genThought, thoughtTime);
    }

    private string ChooseThought(string[] thoughts, int choice)
    {
        if (choice != -1)
        {
            return thoughts[choice];
        }
        else
        {
            int rand = Random.Range(0, thoughts.Length);
            return thoughts[rand];
        }
    }

    public static string NumberToWords(int number) //converts int to words
    {
        if (number == 0)
            return "zero";

        if (number < 0)
            return "minus " + NumberToWords(Mathf.Abs(number));

        string words = "";

        if ((number / 1000000) > 0)
        {
            words += NumberToWords(number / 1000000) + " million ";
            number %= 1000000;
        }

        if ((number / 1000) > 0)
        {
            words += NumberToWords(number / 1000) + " thousand ";
            number %= 1000;
        }

        if ((number / 100) > 0)
        {
            words += NumberToWords(number / 100) + " hundred ";
            number %= 100;
        }

        if (number > 0)
        {
            if (words != "")
                words += "and ";

            var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

            if (number < 20)
                words += unitsMap[number];
            else
            {
                words += tensMap[number / 10];
                if ((number % 10) > 0)
                    words += "-" + unitsMap[number % 10];
            }
        }

        return words;
    }

}
