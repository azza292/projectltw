﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlowOut : MonoBehaviour {

    private LightPost lightPost;

    private void Start()
    {
        lightPost = GetComponentInParent<LightPost>();
    }
    //Wind Detection (Blow out in wind)
    private void OnParticleCollision(GameObject other)
    {
		if (other != null) {
			lightPost.Windy (other);
		}
    }
}
