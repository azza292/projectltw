﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class PauseController : MonoBehaviour {

	[Header("Canvas")]
    public Canvas pauseMenu;
    public Canvas noteMenu;
    public Canvas winMenu;
    public Canvas uiCanvas;
    public Canvas uiInput;

	[Space(30)]
	[Header("Sounds")]
	public AudioClip winSound;
	public AudioClip coinSound;
	public AudioClip openNote;



    private UI_WIN winUI;
	private Animator winAnim;

    private GameController game;
	private UI_AnimationController animUI;
	private AudioSource uiAudio;

    private string currentLevel;
    private string nextLevel;


    private float defaultTime;
    private Camera mainCam;


	private bool newCoinPlaying;

	// Use this for initialization
	void Start () {

        mainCam = Camera.main;

        winUI = winMenu.GetComponent<UI_WIN>(); // Grab UI elements form Canvas 
		winAnim = winUI.GetComponent<Animator>();

        game = GameObject.FindObjectOfType<GameController>();
		animUI = GetComponent<UI_AnimationController> ();

		uiAudio = GetComponent<AudioSource> ();

        currentLevel = SceneManager.GetActiveScene().name;

        if (pauseMenu == null)
        {
            Debug.LogWarning("Warning: Pause Controller needs a menu selected");
        }
			

	}
		
	// ================================================
	// COINS ADITION
	// ================================================
	void Update()
	{
		if (winMenu.gameObject.activeInHierarchy) {
			
			if ((winAnim.GetCurrentAnimatorStateInfo (0).IsName ("LCCoin") && !newCoinPlaying)) {
				StartCoroutine(addCoin(winAnim.GetCurrentAnimatorClipInfo (0).Length, winUI.levelCompleteCoin));
			}
			else if ((winAnim.GetCurrentAnimatorStateInfo (0).IsName ("SSCoin") && !newCoinPlaying)) {
				StartCoroutine(addCoin(winAnim.GetCurrentAnimatorClipInfo (0).Length, winUI.speedCoin));
			}
			else if ((winAnim.GetCurrentAnimatorStateInfo (0).IsName ("RTCoin") && !newCoinPlaying)) {
				StartCoroutine(addCoin(winAnim.GetCurrentAnimatorClipInfo (0).Length, winUI.noBlowCoin));
			}
		}

	}
		
	IEnumerator addCoin(float time, Image img)
	{
		newCoinPlaying = true;
		yield return new WaitForSecondsRealtime (time/2); // REALTIME FOR WHEN TIME SCALE IS 0!
		uiAudio.PlayOneShot (coinSound);
		winUI.currentCoins.text = ((Convert.ToInt32(winUI.currentCoins.text)+1).ToString());
		img.sprite = winUI.tickImage;
		yield return new WaitForSecondsRealtime (time/2); // REALTIME FOR WHEN TIME SCALE IS 0!
		newCoinPlaying = false;
	}

	public void ShowWinMenu(string nextLevel, int coinsEarnt, float winTimeP, float goldTime, bool[] howCoin)
    {
        this.nextLevel = nextLevel;

        if ((winMenu.gameObject.activeInHierarchy == false) && (winMenu != null))
        {
            // HIDE UI AND STOP TIME
            StopTime();
			uiAudio.PlayOneShot (winSound);

            // UPDATE TEXT FIELD WITH CURRENT DATA
            winUI.timeCompleted.text = ConvertToTime(winTimeP);
            winUI.goldTime.text = ConvertToTime(goldTime);

			//SHOW MENU
			if ((winMenu.gameObject.activeInHierarchy == false) && (winMenu != null)) {
				winMenu.gameObject.SetActive (true);
			}
		
			//----------------------------------------------------------------------------------------------------------
			// LOAD DATA
			// --------------------------------------------------------------------------------------------------------
			//LOAD STORED DATA
			float[] prevLevelData = LoadLevelData();
			int[] prevPlayerData = LoadPlayerData();

			int prevCurrentCoins = prevPlayerData[0];
			//float prevTimeTakenOnLevel = prevLevelData[0];
			int coinsCollectedFromLevel = Convert.ToInt32(prevLevelData[1]);

			// Calculate coins to add
			int coinsEarntB = coinsEarnt - coinsCollectedFromLevel;
			if (coinsEarntB < 0) {
				coinsEarntB = 0;
			}
			int currentCoins = prevCurrentCoins + coinsEarntB;


			// SAVING LEVEL DATA
			SaveLevelData(winTimeP, coinsEarnt, game.checkPointCount, howCoin);

			// SAVE PLAYER DATA
			SavePlayerData(currentCoins);

			//-------------------------------------------------------------------------------------------------------



			//-------------------
			// COINS ADDING
			//-------------------


			winUI.currentCoins.text = prevCurrentCoins.ToString();

			//========================
			// LEVEL COMPLETE
			//========================
			if (prevLevelData [0] != 0){ // IF LEVEL COMPLETE BEFORE
				winUI.levelCompleteCoin.sprite = winUI.tickImage;
			} else {
				winAnim.SetTrigger ("LCTrigger");
			}

			//========================
			// SPEEDY
			//========================
			if (Convert.ToBoolean (prevLevelData [3])) { // IF COMPLETED SPEEDY FOR THIS LEVEL
				winUI.speedCoin.sprite = winUI.tickImage;
			} 
			else { // IF NOT DONE SPEEDY BEFORE
				
				if (howCoin [0]) { // IF COMPLETED SPEEDY, PLAY ANIM
					winUI.speedCoin.sprite = winUI.coinImage;
					winAnim.SetTrigger ("SSTrigger");
				} else // IF NOT COMPLETED SPEEDY
				{
					winUI.speedCoin.sprite = winUI.noCoinImage;
				}
			}

			//========================
			// RELENTLESS TORCH
			//========================
			if (Convert.ToBoolean (prevLevelData [4])) { // IF COMPLETED RT FOR THIS LEVEL
				winUI.noBlowCoin.sprite = winUI.tickImage;
			} 
			else { // IF NOT DONE RT BEFORE
				
				if (howCoin [1]) { // IF COMEPLETED RT, PLAY ANIM
					winUI.noBlowCoin.sprite = winUI.coinImage;
					winAnim.SetTrigger ("RTTrigger");
				} else  // IF NOT
				{
					winUI.noBlowCoin.sprite = winUI.noCoinImage;
				}
			}
        }
    }

		

    private string ConvertToTime(float timeConvert)
    {
        TimeSpan winTimeSpan = TimeSpan.FromSeconds(timeConvert);
        string sTime = string.Format("{0:D2}:{1:D2}", winTimeSpan.Minutes, winTimeSpan.Seconds);
        return sTime;
    }

    public void ShowNote(string noteTitle, string noteContents)
    {
		if ((noteMenu.gameObject.activeInHierarchy == false) && (noteMenu != null)) {
			noteMenu.gameObject.SetActive (true);
		}
		uiAudio.PlayOneShot (openNote);
		animUI.OpenNote ();
		StopTime();
        Text[] t_array = noteMenu.GetComponentsInChildren<Text>();
        t_array[0].text = noteTitle;
        t_array[1].text = noteContents;
        
    }
    public void ShredNote()
    {
        if ((noteMenu.gameObject.activeInHierarchy == true) && (noteMenu != null))
        {
            StartTime();
			animUI.CloseNote ();

        }
    }
    public void PauseGame()
    {
		if ((pauseMenu.gameObject.activeInHierarchy == false) && (pauseMenu != null)) {

			pauseMenu.gameObject.SetActive (true);
		}

		pauseMenu.GetComponent<Image> ().raycastTarget = true;

		StopTime ();
		animUI.SlideIn ();

		TimeSpan timeSpan = TimeSpan.FromSeconds (game.timePassedSeconds);
		string timeText = string.Format ("{0:D2}.{1:D2}", timeSpan.Minutes, timeSpan.Seconds);

		Text[] t_array = pauseMenu.GetComponentsInChildren<Text> ();
		t_array [0].text = timeText;

		int[] pData = LoadPlayerData ();
		GameObject.FindGameObjectWithTag ("CurrentCoins").GetComponent<Text> ().text = pData [0].ToString ();


    }
    public void ResumeGame()
    {

		pauseMenu.GetComponent<Image> ().raycastTarget = false;
		animUI.SlideOut ();
		StartTime ();
    }

    public void StopTime()
    {
		AudioControl(false);
        Time.timeScale = 0; // stop time
		Screen.sleepTimeout = 2; // Timeout to 2 seconds while paused

        uiCanvas.GetComponent<UI_TEXT>().thoughtDisplay.gameObject.SetActive(false); // Disable thought Display
        mainCam.transform.gameObject.GetComponent<Blur>().enabled = true; // enable blur
        uiCanvas.gameObject.SetActive(false); // Disable UI
        uiInput.gameObject.SetActive(false); // Disable UI Input

    }
    public void StartTime()
    {
		AudioControl(true);
        Time.timeScale = 1; // start time
		Screen.sleepTimeout = SleepTimeout.NeverSleep; // SET TIMEOUT TO NO!

        uiCanvas.GetComponent<UI_TEXT>().thoughtDisplay.gameObject.SetActive(true); // Enable thought display
        mainCam.transform.gameObject.GetComponent<Blur>().enabled = false; // disable blur
        uiCanvas.gameObject.SetActive(true); // enable UI
        uiInput.gameObject.SetActive(true); // enable UI Input

    }
		
	private AudioSource[] allAudioSources;

	void AudioControl(bool decision) {
		allAudioSources = FindObjectsOfType<AudioSource>() as AudioSource[];
		foreach( AudioSource audioS in allAudioSources) {
			audioS.mute = !decision;
			uiAudio.mute = false;
		}
	}
		


    // ==============================================================================
    // LEVEL DATA
    // ==============================================================================

	public void SaveLevelData(float winTime, int medal, int checkPointCount, bool[]howCoin)
    {
        SaveLoadManager.SaveLevelData(winTime, medal, checkPointCount, howCoin, currentLevel);
    }
    public float[] LoadLevelData()
    {
        float[] newData = SaveLoadManager.LoadLevelData(currentLevel);
        return newData;
    }

    // ==============================================================================
    // PLAYER DATA
    // ==============================================================================

    public void SavePlayerData(int coinsCollected)
    {
        SaveLoadManager.SavePlayerData(coinsCollected);
    }
    public int[] LoadPlayerData()
    {
        int[] pData = SaveLoadManager.LoadPlayerData();
        return pData;
    }



    // ========================================================================
    // BUTTONS
    // ========================================================================

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void NextLevel()
    {
        SceneManager.LoadScene(nextLevel);
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
