using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Vector3 m_Move;
        private bool m_Jump;

        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private VirtualJoystick joystick;

        private void Start()
        {
            joystick = FindObjectOfType<VirtualJoystick>(); //grab joystick

            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning("No Main Camera Found");
            }

            m_Character = GetComponent<ThirdPersonCharacter>();
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            float h = joystick.Horizontal();
            float v = joystick.Vertical();

			// FOOT STEPS
			if (m_Character.m_IsGrounded) {
				GetComponent<Player> ().ChooseStepSound (h, v);
			}

			bool crouch = false;
			m_Jump = false;
            //bool crouch = Input.GetKey(KeyCode.C);
            //m_Jump = Input.GetKey(KeyCode.Space);


            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v * m_CamForward + h * m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v * Vector3.forward + h * Vector3.right;
            }

            
            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, m_Jump);
        }
    }
}
