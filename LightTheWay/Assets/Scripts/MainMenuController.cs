﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    public Canvas settingsMenu;
    public Canvas clearDataDialogBox;
    public Canvas levelSelect;

	private UI_AnimationController animUI;

    // Use this for initialization
    void Start()
    {
		animUI = GetComponent<UI_AnimationController> ();

        Time.timeScale = 1; // for loading levels
        GetCoins();

    }

	public void GetCoins()
	{
		int[] pData = SaveLoadManager.LoadPlayerData();
		GameObject.FindGameObjectWithTag("CurrentCoins").GetComponent<Text>().text = pData[0].ToString();
	}

    public void OpenSettings()
    {
        if ((settingsMenu.gameObject.activeInHierarchy == false) && (settingsMenu != null))
        {
            settingsMenu.gameObject.SetActive(true);
        }
		animUI.MainMenuClose ();
		animUI.SlideOut ();
		animUI.SettingsOpen ();
    }
	public void CloseSettings()
	{
		animUI.SlideIn ();
		animUI.SettingsClose ();
		animUI.MainMenuOpen ();

	}
		
    public void SelectLevel()
    {
        if ((levelSelect.gameObject.activeInHierarchy == false) && (levelSelect != null))
        {
            levelSelect.gameObject.SetActive(true);
        }
		animUI.MainMenuClose ();
		animUI.SlideOut ();
		animUI.LevelSelectOpen ();
    }

	public void CloseLevels()
	{
		animUI.SlideIn ();
		animUI.LevelSelectClose ();
		animUI.MainMenuOpen ();

	}


    // ==============================================================================
    // CLEAR DATA
    // ==============================================================================
    private void ClearData()
    {
            foreach (string file in System.IO.Directory.GetFiles(Application.persistentDataPath + "/", "*.ltw"))
            {
                File.Delete(file);
            }
            GetCoins(); // Refresh Coin Amount

    }
    public void ToggleClearDataDialog()
    {
        if ((clearDataDialogBox.gameObject.activeInHierarchy == false) && (clearDataDialogBox != null))
        {
            clearDataDialogBox.gameObject.SetActive(true);
        }
        else if (clearDataDialogBox != null)
        {
            clearDataDialogBox.gameObject.SetActive(false);
        }
    }



    // ==============================================================================
    // BUTTONS
    // ==============================================================================

    public void ExitGame()
    {
        Application.Quit();
    }
    public void ClearDataDialogYes()
    {
        ClearData();
        ToggleClearDataDialog();
    }
    public void ClearDataDialogNo()
    {
        ToggleClearDataDialog();
    }

}
