﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public float damping = 1;
	public float objHighlightTime = 3f;
    public Transform player = null;
    private Transform cam = null;

	private bool objectView = false;
	private Vector3 newPos;
	private Transform objToView;
	private float camFOV;
	private float newFOV;

	public Vector3 camOffset;


    public void Start()
    {
		camFOV = Camera.main.fieldOfView;

        cam = transform;

		if(Camera.main.orthographic){
			camOffset = new Vector3 (0, 0, 0);
		}
    }
    public void Update()
    {
		DungeonCam ();
    }
	private void DungeonCam()
	{
		if (!objectView) {
			// FOLLOW PLAYER
			newPos = Vector3.Lerp (transform.position, player.position + camOffset, Time.deltaTime * damping);
			Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, camFOV, Time.deltaTime * damping);

		} else {
			// HIGHLIGHT OBJ
			newPos = Vector3.Lerp (transform.position, objToView.position + camOffset, Time.deltaTime * damping);
			Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, newFOV, Time.deltaTime * damping);

		}
		cam.position = newPos;
	}
	public void HightlightObject(Transform objToView, float newFOV)
	{
		if (newFOV == 0) {
			this.newFOV = camFOV;
		} else {
			this.newFOV = newFOV;
		}
		this.objToView = objToView;
		objectView = true;
		StartCoroutine( HighlightTime ());
	}

	public void HighlightPlayer()
	{
		objectView = false;
	}

	public void ShakeCam(float dur, float mag, float delay)
	{
		StartCoroutine(Shake (dur, mag, delay));
	}

	IEnumerator HighlightTime()
	{
		yield return new WaitForSeconds(objHighlightTime);
		objectView = false;
	}


	IEnumerator Shake(float duration, float magnitude, float delay) {

		float elapsed = 0.0f;

		while (elapsed < duration+delay) {

			elapsed += Time.deltaTime;
			if (elapsed > delay) {
				Camera.main.transform.position = Camera.main.transform.position + Random.insideUnitSphere * magnitude;
				yield return null;
			} else {
				yield return null;
			}
		}
	}
}


