﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour {

    public string prevNameExplicit;
    public string levelNameExplicit;
    public int levelNo;
    public bool startUnLocked;

    private bool locked;
    private float[] prevLoadedData;
    private float[] loadedData;

    public Image lockedPanel;
    public Text levelNumber;
    public Text completedTime;

	// Use this for initialization
	void OnEnable () {

        prevLoadedData = SaveLoadManager.LoadLevelData(prevNameExplicit);
        loadedData = SaveLoadManager.LoadLevelData(levelNameExplicit);

        levelNumber.text = levelNo.ToString();

        // If havnt completed this level and startUnlocked isnt true
        if (loadedData[0] == 0 && !startUnLocked)
        {
            locked = true;
            lockedPanel.gameObject.SetActive(true);

            //If previous level is unlocked
            if (prevLoadedData[0] != 0)
            {
                locked = false;
                lockedPanel.gameObject.SetActive(false);
                completedTime.text = chkLvlTime();
            }
        }
        else
        {
            //Unlock level
            locked = false;
            //Display LVL Number
            completedTime.text = chkLvlTime();
        }



    }

    private string chkLvlTime()
    {
        //If Level time is 0 display N/A
        if (loadedData[0] != 0)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(loadedData[0]);
            string timeText = string.Format("{0:D2}.{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
            return timeText;
        }
        else
        {
            return "N/A";
        }
    }

    public void PlayLevel()
    {
        if (!locked)
        {
            SceneManager.LoadScene(levelNameExplicit);
        }
    }
}
