﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    private Rigidbody doorRigid;

    public bool startLocked;
    public bool isLocked;
    public bool wasUnLocked = false;

    public float closeAngle = 60f;
	public float clipAngle = 0f;

	private float currentAngle;
    private bool isTouchingCollider;
	public bool autoClose;
	public float speed;

	[Space(10)]
	[Header("Audio")]
	[Space(10)]
	private AudioSource audioSrc;
	public AudioClip[] openSounds;
	public AudioClip closeSound;


    // Use this for initialization
    void Start()
    {
		// IF NOT SPECIFIED CLIP ANGLE, STARTING POS WILL BE USED
		if (clipAngle == 0) {
			
			clipAngle = transform.localRotation.eulerAngles.y;
		}

		//Audio Source
		audioSrc = GetComponent<AudioSource>();

        doorRigid = GetComponent<Rigidbody>();
        if (startLocked || doorRigid.isKinematic) {  isLocked = true; startLocked = true; doorRigid.isKinematic = true; }
    }


    public void Lock(bool decise)
    {
        doorRigid.isKinematic = decise;
        isLocked = decise;
    }
    private void Update()
    {
        CheckMoving();
    }
    private void CheckMoving()
    {
        currentAngle = transform.localRotation.eulerAngles.y;
        speed = doorRigid.velocity.magnitude;
		if ((currentAngle > clipAngle - closeAngle) && (currentAngle < clipAngle + closeAngle) && (speed < 0.1f) && (!isTouchingCollider) &&(autoClose)) { // is bettween to value (almost closed) is not moving and is not touching a collider
			Quaternion goRot = Quaternion.Euler (0, clipAngle, 0); // set final rotation
			transform.localRotation = Quaternion.Slerp (transform.localRotation, goRot, Time.deltaTime * 6); // move to rotation

			// If essentially closed with a threshold value, stop this function to save memory and play sound
			if ((transform.localRotation.eulerAngles.y > clipAngle - 2)&&(transform.localRotation.eulerAngles.y < clipAngle + 2))
			{
				autoClose = false;
				audioSrc.PlayOneShot (closeSound);
			}
		} 
    }
	private void OnCollisionEnter(Collision collision)
	{
		// Check if player is opening and door is moving
		if (collision.gameObject.tag == "Player" && doorRigid.velocity.magnitude > 0.2f) 
		{
			autoClose = true;
			int randDoorSound = Random.Range (0, openSounds.Length);
			audioSrc.volume = 0.1f;
			audioSrc.PlayOneShot (openSounds [randDoorSound]);
		}
	}
    private void OnCollisionStay(Collision collision)
    {
        isTouchingCollider = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        isTouchingCollider = false;
    }
}
