﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_WIN : MonoBehaviour {

    // NEW INFO
    public Text timeCompleted;

    // REWARD TIMES
    public Text goldTime;

    // PANELS
    public Image levelComplete;
	public Image speedyRun;
	public Image noBlowOut;

	public Image levelCompleteCoin;
	public Image speedCoin;
	public Image noBlowCoin;

	public Sprite coinImage;
	public Sprite noCoinImage;
	public Sprite tickImage;
    //EXISTING INFO
    public Text currentCoins;
}
