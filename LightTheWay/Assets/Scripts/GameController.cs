﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    private WindController navAgent; // gets nav agent
    private float windCycleTime;

    [Space(30)]
    [Header("Player Info")]
    public Player player; // get player class
    public UI_TEXT ui; // ui elements
    private PlayerThoughts thinking; // player think class
    public float thinkDuration = 3.5f;

    [Space(30)]
    [Header("Time and Winning Info")]
    public bool hasWon; // has player won
    public float timePassedSeconds;
    public float winTime;

    [Space(30)]
    [Header("Extra Coins and Next Level")]
    public float extraCoinTime = 45f;
    public int coins;

    public string nextLevel = "MainMenu";
    private PauseController pause;

    [Space(30)]
    [Header("Check Points")]

    private GameObject checkPointContainer; // container of checkpoints
    public LightPost[] checkPoints; // array of checkpoit lights
    public int checkPointCount; // count on checkpoints in container
    public int actualLitCount; // current number of lit checkpoints

    [Space(30)]
    [Header("Performance")]
    //RESOLUTION and PERFORMANCE
    public int targetFrameRate; //target frame rate


    // Use this for initialization
    void Start () {

        pause = FindObjectOfType<PauseController>();
		navAgent = FindObjectOfType<WindController>();
        player = FindObjectOfType<Player>();

        thinking = GetComponent<PlayerThoughts>();

        hasWon = false; // You havnt won yet!

		grabCheckPointContainer();

        Time.timeScale = 1; // for loading levels
    }
	void grabCheckPointContainer()
    {
        checkPointContainer = GameObject.FindGameObjectWithTag("CheckPointContainer");

        if (checkPointContainer != null)
        {
            getCheckPoints();// populates array with checkpoints with parent object
        }
        else { Debug.LogError("Error: Please attach checkpoint container to GameController"); }
    }
	// Update is called once per frame
	void Update () {

        //IF NOT WON YET
        if(!hasWon)
        {
            timePassedSeconds += Time.deltaTime; // ADD TO TIMER

            if (checkCheckPoints())
            {
                Win();
            }
        }
        
    }

    void getCheckPoints() // populates array with checkpoints with parent object
    {
        //  THIS FUNCTION COUNTS THE ACTIVE CHILDREN IN CONTAINER TO CREATE THE CORRECT ARRAY LENGTH
        for (int c = 0; c < checkPointContainer.transform.childCount; c++)
        {
            if (checkPointContainer.transform.GetChild(c).gameObject.activeInHierarchy)
            {
                checkPointCount++;
            }
        }

        // THIS FUNCITION POPULATES THE ARRAY WITH ALL THE ACTIVE CHILDREN
        checkPoints = new LightPost[checkPointCount];
        for (int i = 0; i < checkPointCount; i++)
        {
            if (checkPointContainer.transform.GetChild(i).gameObject.activeInHierarchy)
            {
                checkPoints[i] = checkPointContainer.transform.GetChild(i).gameObject.GetComponent<LightPost>();
            }
        }

    }

    bool checkCheckPoints() //counts lit checkpoints and compares with lengh of array
    {
        windCycleTime = navAgent.prevTimeToTarget;
        int tempCurrentCount = 0;
        int calcCount = 0; // actual Chkpts lit longer than the wind's cycle


        for (int i = 0; i < checkPointCount; i++) // chk every checkpoint
        {
            if (checkPoints[i].isLit) // if checkpoint is li
            {
                tempCurrentCount++; // how many are lit
            }

            if (checkPoints[i].timeSpentLit > windCycleTime) //  time spent lit is greater than the wind cycle time
            {
                calcCount++;
            }
        }

        actualLitCount = tempCurrentCount; // actaul lit eqauls number lit
        if ((tempCurrentCount == checkPointCount)&&(calcCount == checkPointCount)) // if count of LIT Chkps is equal to number of Chkps stored in array | if calculated count(lit longer than wind cycle time) is equal to number of Chkps stored in array 
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    void Win() //WINNER
    {
        hasWon = true; // You've won
        thinking.thoughtGenerator(99);


        winTime = timePassedSeconds - windCycleTime; //When you actually won
        bool[] howCoin = CalcCoins();

        StartCoroutine(LoadNewLevel(howCoin));
    }

	bool[] CalcCoins()
    {
		bool[] howCoin =  new bool[2]; // SET BOOL FOR UI FUNCTIONS

		coins++; // ONE COIN FOR LEVEL COMPLETEION

		if(winTime <= extraCoinTime) { coins ++; howCoin [0] = true;} // ONE COIN FOR SPEEDY LEVEL

		if(! player.hasBeenBlown){coins++; howCoin [1] = true;} // ONE COIN IF PLAYERS TORCH DOESNT GO OUT

		return howCoin;
    }

	IEnumerator LoadNewLevel(bool[] howCoin)
    {
        yield return new WaitForSeconds(5f);
        pause.ShowWinMenu(nextLevel, coins, winTime, extraCoinTime, howCoin);
    }

}
