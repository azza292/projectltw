﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_TEXT : MonoBehaviour {

    // Use this for initialization
    private GameController game;

    public Text fpsDisplay;
    public Text timerDisplay;

    public Text thoughtDisplay;
	private Animator thoughtsUI;


	public Slider checkPointSlider;
	public GameObject divider;

    private float fps;
    private float t_thoughtTime; //local thought time
    private float t_thinkDuration; // local think duration

    void Start () {

		game = GameObject.FindObjectOfType<GameController> ();
		thoughtsUI = thoughtDisplay.GetComponent<Animator> ();
        t_thinkDuration = game.thinkDuration; //grab think duration from game class
		SetupCheckPointSlider();
    }

	// Update is called once per frame
	void Update () {

        updateCheckPointDisplay();
        updateTimerDisplay();
        updateFpsDisplay();
        clearThoughts();
    }



    void updateFpsDisplay()
    {
        fps = 1 / Time.deltaTime;
        fpsDisplay.text = (fps.ToString("F2"));
    }

    void updateTimerDisplay()
    {
        TimeSpan timeSpan = TimeSpan.FromSeconds(game.timePassedSeconds);
        string timeText = string.Format("{0:D2}.{1:D2}",timeSpan.Minutes, timeSpan.Seconds);
        timerDisplay.text = timeText;
    }

	void SetupCheckPointSlider()
	{

		checkPointSlider.maxValue = game.checkPointCount;
		checkPointSlider.minValue = 0;

		float sWidth = checkPointSlider.GetComponent<RectTransform> ().rect.width;
		float sHeight = checkPointSlider.GetComponent<RectTransform> ().rect.height;
		float sStartX = checkPointSlider.transform.position.x - sWidth / 2;
		float sMiddleY = (checkPointSlider.transform.position.y);
		float sStep = sWidth / game.checkPointCount;

		Quaternion rot = new Quaternion (0, 0, 0, 0);
		float cX = sStep;
		for (int i = 0; i < game.checkPointCount; i++) {

			if (i != game.checkPointCount - 1) {
				Vector3 pos = new Vector3 (sStartX + cX, sMiddleY, checkPointSlider.transform.position.z);
				Instantiate (divider, pos, rot, checkPointSlider.transform);
				cX += sStep;
			}

		}

	}
    void updateCheckPointDisplay()
    {
		checkPointSlider.value = Mathf.Lerp(checkPointSlider.value, game.actualLitCount, Time.deltaTime * 3);
    }
		
    public void updateThoughtDisplay(string thought, float thoughtTime)
    {
        this.t_thoughtTime = thoughtTime; // local TT is TT
        thoughtDisplay.text = ('"' + thought + '"'); //add quotes to text
		thoughtsUI.Play("open");
    }

    void clearThoughts() // clear thoughts
    {
		if ((game.timePassedSeconds - t_thoughtTime > t_thinkDuration)&& (t_thoughtTime != 0)) { // if the thought has been displayed on screen for correct time
			
			if(!(thoughtsUI.GetCurrentAnimatorStateInfo (0).IsName ("none"))){ // To stop it playing after game is paused
				thoughtsUI.Play ("close");
				t_thoughtTime = 0;}
		}
    }

}
