﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour {

    private PlayerThoughts think;
    public GameObject gate;
	public float camFOV = 45f;

	private float plateStartPos;
    private float gateStartPos;

    private bool isPressed;
	private bool hasOpened;
	private bool hasClosed;


	private FollowPlayer mainCam;

	private AudioSource audioSrc;

	public AudioClip openSound;
	public AudioClip openComplete;
	public AudioClip closeSound;
	public AudioClip closeComplete;
	private int soundInt; // 1 for rise, 2 for fall, 3 for stop, 0 for nothing
	// Use this for initialization
	void Start () {
		
		isPressed = false;
		if (gate == null) {
			Debug.LogError ("Pressure Plate has no gate selected");
		}
        think = FindObjectOfType<PlayerThoughts>();

		mainCam = Camera.main.GetComponent<FollowPlayer> ();
		audioSrc = gate.GetComponent<AudioSource> ();

        gateStartPos = gate.transform.position.y;
		plateStartPos = transform.localPosition.y;



	}
	
	// Update is called once per frame
	void Update () {

		// IF PRESSURE PLATE IS PRESSED
		if (isPressed) {

			//IF GATE HAS NOT OPENED ALREADY, OPEN
			if(!hasOpened) liftGate ();

		} else {

			//IF GATE HAS NOT CLOSED ALREADY, CLOSE
			if (!hasClosed) dropGate ();

		}

	}


	private void PlaySounds(AudioClip sound, bool overide)
	{
		if (!audioSrc.isPlaying || overide) {
			audioSrc.Stop ();
			audioSrc.PlayOneShot (sound);
			overide = false;
		}
	}
		
    public void OnTriggerEnter(Collider other)
    {
		if (other.tag == "Player" && !isPressed) {

			// Move Cam to show gate
			mainCam.HightlightObject (gate.transform, camFOV);
			think.thoughtGenerator (7);
		}
		else if (other.tag == "WoodenBox"  && !isPressed)
        {
            think.thoughtGenerator(8);
        }
    }

    public void OnTriggerStay(Collider other)
	{
		if (other.tag == "WoodenBox" || other.tag == "Player")
		{
			isPressed = true;
        }
    }
	public void OnTriggerExit(Collider other)
    {
		// WHEN PLAYER LEAVES PRESSURE PLATE, FOCUS CAMERA ON PLAYER
		if (other.tag == "Player" && isPressed) {
			mainCam.HighlightPlayer ();
		}
			isPressed = false;
    }
    private void liftGate()
    {
			//PRESSES PLATE
		if (transform.localPosition.y > 0.45f) {
			transform.Translate (Vector3.down * Time.deltaTime / 2f, Space.Self); // move over time
		}

			//LIFTS GATE
		//Debug.Log(gate.transform.localPosition.y);
		if (gate.transform.position.y < 7) { // Move gate up
			gate.transform.Translate (Vector3.up * Time.deltaTime * 0.9f, Space.Self); // move over time
			hasClosed = false;
			PlaySounds(openSound, false);
		}
		else{

			//GATE OPEN
			hasOpened = true;
			PlaySounds (openComplete, true);

		}
				
		
			
    }
    private void dropGate()
    {
			//UN-PRESSES PLATE
		if (transform.localPosition.y < plateStartPos) {
			transform.Translate (Vector3.up * Time.deltaTime / 2f, Space.Self); // move over time
		}

			// DROPS GATE
		if (gate.transform.position.y > gateStartPos) {
			gate.transform.Translate (Vector3.down * Time.deltaTime * Physics.gravity.magnitude, Space.Self); // move over time
			hasOpened = false;
			PlaySounds(closeSound, false);
		} else {
			
			//GATE CLOSED
			hasClosed = true;

			// DONT PLAY SLAM ON STARTUP
			if(gateStartPos != gate.transform.position.y){ 
				PlaySounds (closeComplete, true);
				Camera.main.GetComponent<FollowPlayer> ().ShakeCam (0.1F, 0.2F, 0.1F);

				// IF IT HAS A DUST SYSTEM< PLAY
				if (gate.GetComponentInChildren<ParticleSystem> ()) {
					gate.GetComponentInChildren<ParticleSystem> ().Emit (50);
				}
			}

		}
    }
}
