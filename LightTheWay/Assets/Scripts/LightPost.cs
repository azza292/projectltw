﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPost : MonoBehaviour {
	
	private Player player;
	private int psCount;
    private PlayerThoughts think; // ref player think
	private Light lampPostLight;

    private ParticleSystem flame;
    private ParticleSystem flame2;
    private ParticleSystem highligher;

	private AudioSource[] audioSrc;

    public ParticleSystem[] flameArray;
    public bool startEnabled; //Start with this flame on?
    public bool isLit; // Is this flame lit fam?
    public bool timer;
    public float timeSpentLit;

	[Space(10)]
	[Header("Audio")]
	[Space(10)]
	public AudioClip litSound;
	public AudioClip burningSound;
	public AudioClip blowOutSound;



    // Use this for initialization
    void Start () {
        //Grabs Light Child
        lampPostLight = GetComponentInChildren<Light>();
        
		//Grabs AAudio Srouce
		audioSrc = GetComponents<AudioSource>();
		audioSrc [0].volume = 0.7f;

        //Grabs flame particle systems 
        flameArray = new ParticleSystem[3]; //set array size to 2
        flameArray = gameObject.GetComponentsInChildren<ParticleSystem>(); // fill array with 3 PS
        flame = flameArray[0].gameObject.GetComponent<ParticleSystem>(); //grab 1
        flame2 = flameArray[1].gameObject.GetComponent<ParticleSystem>(); //grab 2
        highligher = flameArray[2].gameObject.GetComponent<ParticleSystem>(); //grab HL

        //grabs thinking system
        think = FindObjectOfType<PlayerThoughts>();


        if (startEnabled)
        { //Turns on Light and Flame if enabled on start (INSPECTOR)
            checkPointStatus(true); timer = true;
        }
        else
        {//Turns off Light and Flame
            checkPointStatus(false); timer = false;
        }


    }
    private void Update()
    {
        if(timer)
        {
            timeSpentLit += Time.deltaTime; // Add to timer
        }

		SoundCheckIfLit ();
    }
    // Lighting Torches
    private void OnTriggerEnter(Collider other)
    {
        //If player enters trigger zone 
        if (other.gameObject.tag == "Player")
        {
            player = other.gameObject.GetComponent<Player>();


            //If players torch is lit, light this checkpoint
            if (player.isEnabled)
            {
                if (!isLit) //If its not already lit
                {
					//Camera.main.GetComponent<FollowPlayer> ().ShakeCam (0.3F, 0.2F);
					PlaySounds (litSound);
                    checkPointStatus(true);
                    think.thoughtGenerator(2);
                }
            }
            //If post is lit, light the players torch
            if (isLit)
            {
                if (!player.isEnabled) // if player isnt already lit
                {
                    player.flameStatus(true);
					player.hasBeenBlown = true; // SET THIS FOR SCORING PURPOSES
                    think.thoughtGenerator(3);
                }
            }

        }

    }
	// IF LIGHT POST IS LIT.. PLAY THE SOUND, IF NOT STOP IT
	private void SoundCheckIfLit() 
	{
		if (isLit && !audioSrc [0].isPlaying) {
			audioSrc [0].clip = burningSound;
			audioSrc [0].Play ();
		} else if (!isLit) {
			audioSrc [0].Stop ();
		}

	}
	//PLAYS ON THE SECONDARY AUDIO SOURCE
	private void PlaySounds(AudioClip sound)
	{
		audioSrc[1].PlayOneShot (sound);
	}

    // CALLED BY WIND ZONE , PASSED PARTICLE COLLISION OBJECT
    public void Windy(GameObject other)
    {
        if (other.gameObject.tag == "WindParticle" && isLit) // if its the wind particles
        {
			PlaySounds (blowOutSound);
            checkPointStatus(false);
        }
    }

    private void checkPointStatus(bool decise) // checkpoint status
    {
        isLit = decise; 
        psSystem(decise);
        lampPostLight.enabled = decise;

        if (decise) // if flame was turned ON
        {
            timer = true; //start timer (how long has chkpnt been lit)
        }
        else
        {
            timeSpentLit = 0f; // reset timer as flame is out
            timer = false;
        }
    }

    void psSystem(bool decise) //grabs particle system UNITY 5
    {
        var em = flame.emission; // INITIAL FLAME
        em.enabled = decise;

        var em2 = flame2.emission; // FLAME GLOW
        em2.enabled = decise;

        var hl = highligher.emission; // Highligher glow
        hl.enabled = !decise;
    }

}
